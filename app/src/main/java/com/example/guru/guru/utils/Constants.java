package com.example.guru.guru.utils;

/**
 * Created by devrath.rathee on 22-06-2017.
 */

public class Constants {

    public static final String GURU_LIST_URL = "http://23.239.206.67/~howguru/index.php/apis/guru/get_all_gurus";
    public static final String DAILY_VIDEO_URL = "http://23.239.206.67/~howguru/index.php/apis/predictions/get_daily_videos";
}
