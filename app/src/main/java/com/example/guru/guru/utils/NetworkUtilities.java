package com.example.guru.guru.utils;

/**
 * Created by santosh.patar on 06-08-2015.
 */


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;



public class NetworkUtilities {

    public static final int MY_SOCKET_TIMEOUT_MS = 1 * 90 * 1000; // m1.30min

    /**
     * Check Internet connection.
     */
    public static boolean isInternet(Context context) {
        boolean isInternet = false;

        ConnectivityManager con = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (con.getNetworkInfo(0) != null && con.getNetworkInfo(1) != null) {

            if (con.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
                    || con.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
                    || con.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED
                    || con.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING) {
                isInternet = true;

            } else if (con.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
                    || con.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {
               /* Toast.makeText(context, context.getString(R.string.no_neteork),
                        Toast.LENGTH_LONG).show();*/
                isInternet = false;

            }
        } else if (con.getNetworkInfo(0) != null) {

            if (con.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
                    || con.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING) {
                isInternet = true;

            } else if (con.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED) {
                /*Toast.makeText(context, context.getString(R.string.no_neteork),
                        Toast.LENGTH_LONG).show();*/
                isInternet = false;

            }
        } else if (con.getNetworkInfo(1) != null) {

            if (con.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED
                    || con.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING) {
                isInternet = true;

            } else if (con.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {
                /*Toast.makeText(context, context.getString(R.string.no_neteork),
                        Toast.LENGTH_LONG).show();*/
                isInternet = false;

            }
        }
        return isInternet;
    }


}
