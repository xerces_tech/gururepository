package com.example.guru.guru.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.guru.guru.utils.Constants;
import com.example.guru.guru.beans.GuruBean;
import com.example.guru.guru.utils.GuruGridView;
import com.example.guru.guru.utils.NetworkUtilities;
import com.example.guru.guru.R;
import com.example.guru.guru.adapters.GuruGridAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class Guructivity extends AppCompatActivity {

    RequestQueue requestQueue;
    GuruGridView guruGridView;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guructivity);

        RelativeLayout headerRelativeLayout = (RelativeLayout) findViewById(R.id.profileHeader_GuruActivity);
        (headerRelativeLayout.findViewById(R.id.appNameTextView)).setVisibility(View.GONE);
        (headerRelativeLayout.findViewById(R.id.profileTextView)).setVisibility(View.VISIBLE);
        ((TextView)headerRelativeLayout.findViewById(R.id.profileTextView)).setText("Personalized Horoscope");

        findViewById(R.id.backToPreviousImageView).setVisibility(View.VISIBLE);
        findViewById(R.id.backToPreviousImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        guruGridView = (GuruGridView) findViewById(R.id.guruGridView_GridActivity);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);
        requestQueue = Volley.newRequestQueue(this);

        if(NetworkUtilities.isInternet(this))
            gsonGetRequest();

        else
            Toast.makeText(this,"Check Internet Connectivity.",Toast.LENGTH_SHORT).show();

    }



    private void gsonGetRequest()
    {
        progressDialog.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Constants.GURU_LIST_URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        ArrayList<GuruBean> guruBeanArrayList = new Gson().fromJson(response.toString(), new TypeToken<List<GuruBean>>(){}.getType());
                        GuruGridAdapter adapter = new GuruGridAdapter(Guructivity.this,guruBeanArrayList);
                        guruGridView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.i("Error : ",error.toString());
                    }
                });
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }
}
