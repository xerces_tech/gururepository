package com.example.guru.guru.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.guru.guru.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        RelativeLayout headerRelativeLayout = (RelativeLayout) findViewById(R.id.aboutHeader);
        (headerRelativeLayout.findViewById(R.id.appNameTextView)).setVisibility(View.GONE);
        (headerRelativeLayout.findViewById(R.id.profileTextView)).setVisibility(View.VISIBLE);
        ((TextView)headerRelativeLayout.findViewById(R.id.profileTextView)).setText("About");

        findViewById(R.id.backToPreviousImageView).setVisibility(View.VISIBLE);
        findViewById(R.id.backToPreviousImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
