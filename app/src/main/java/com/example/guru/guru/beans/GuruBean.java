package com.example.guru.guru.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by devrathrathee on 11/06/17 AD.
 */

public class GuruBean implements Parcelable {


    int id;
    String guru_email,guru_photo,guru_name,guru_address,
           guru_city,guru_country,guru_zip_code,guru_description,
            guru_services,guru_experience;
    float guru_rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGuru_email() {
        return guru_email;
    }

    public void setGuru_email(String guru_email) {
        this.guru_email = guru_email;
    }

    public String getGuru_photo() {
        return guru_photo;
    }

    public void setGuru_photo(String guru_photo) {
        this.guru_photo = guru_photo;
    }

    public String getGuru_name() {
        return guru_name;
    }

    public void setGuru_name(String guru_name) {
        this.guru_name = guru_name;
    }

    public String getGuru_address() {
        return guru_address;
    }

    public void setGuru_address(String guru_address) {
        this.guru_address = guru_address;
    }

    public String getGuru_city() {
        return guru_city;
    }

    public void setGuru_city(String guru_city) {
        this.guru_city = guru_city;
    }

    public String getGuru_country() {
        return guru_country;
    }

    public void setGuru_country(String guru_country) {
        this.guru_country = guru_country;
    }

    public String getGuru_zip_code() {
        return guru_zip_code;
    }

    public void setGuru_zip_code(String guru_zip_code) {
        this.guru_zip_code = guru_zip_code;
    }

    public String getGuru_description() {
        return guru_description;
    }

    public void setGuru_description(String guru_description) {
        this.guru_description = guru_description;
    }

    public String getGuru_services() {
        return guru_services;
    }

    public void setGuru_services(String guru_services) {
        this.guru_services = guru_services;
    }

    public float getGuru_rating() {
        return guru_rating;
    }

    public void setGuru_rating(float guru_rating) {
        this.guru_rating = guru_rating;
    }


    public String getGuru_experience() {
        return guru_experience;
    }

    public void setGuru_experience(String guru_experience) {
        this.guru_experience = guru_experience;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.guru_email);
        dest.writeString(this.guru_photo);
        dest.writeString(this.guru_name);
        dest.writeString(this.guru_address);
        dest.writeString(this.guru_city);
        dest.writeString(this.guru_country);
        dest.writeString(this.guru_zip_code);
        dest.writeString(this.guru_description);
        dest.writeString(this.guru_services);
        dest.writeString(this.guru_experience);
        dest.writeFloat(this.guru_rating);
    }

    public GuruBean() {
    }

    protected GuruBean(Parcel in) {
        this.id = in.readInt();
        this.guru_email = in.readString();
        this.guru_photo = in.readString();
        this.guru_name = in.readString();
        this.guru_address = in.readString();
        this.guru_city = in.readString();
        this.guru_country = in.readString();
        this.guru_zip_code = in.readString();
        this.guru_description = in.readString();
        this.guru_services = in.readString();
        this.guru_experience = in.readString();
        this.guru_rating = in.readFloat();
    }

    public static final Creator<GuruBean> CREATOR = new Creator<GuruBean>() {
        @Override
        public GuruBean createFromParcel(Parcel source) {
            return new GuruBean(source);
        }

        @Override
        public GuruBean[] newArray(int size) {
            return new GuruBean[size];
        }
    };
}
