package com.example.guru.guru.beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by devrathrathee on 13/06/17 AD.
 */

public class SunshineBean implements Parcelable {



    int zodiac_id,zodiacImage;
    String zodiac_name,zodiac_symbol,zodiac_file_path,zodiac_image_path;
    public boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getZodiac_id() {
        return zodiac_id;
    }

    public void setZodiac_id(int zodiac_id) {
        this.zodiac_id = zodiac_id;
    }

    public int getZodiacImage() {
        return zodiacImage;
    }

    public void setZodiacImage(int zodiacImage) {
        this.zodiacImage = zodiacImage;
    }

    public String getZodiac_name() {
        return zodiac_name;
    }

    public void setZodiac_name(String zodiac_name) {
        this.zodiac_name = zodiac_name;
    }

    public String getZodiac_symbol() {
        return zodiac_symbol;
    }

    public void setZodiac_symbol(String zodiac_symbol) {
        this.zodiac_symbol = zodiac_symbol;
    }

    public String getZodiac_file_path() {
        return zodiac_file_path;
    }

    public void setZodiac_file_path(String zodiac_file_path) {
        this.zodiac_file_path = zodiac_file_path;
    }

    public String getZodiac_image_path() {
        return zodiac_image_path;
    }

    public void setZodiac_image_path(String zodiac_image_path) {
        this.zodiac_image_path = zodiac_image_path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.zodiac_id);
        dest.writeInt(this.zodiacImage);
        dest.writeString(this.zodiac_name);
        dest.writeString(this.zodiac_symbol);
        dest.writeString(this.zodiac_file_path);
        dest.writeString(this.zodiac_image_path);
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
    }

    public SunshineBean() {
    }

    protected SunshineBean(Parcel in) {
        this.zodiac_id = in.readInt();
        this.zodiacImage = in.readInt();
        this.zodiac_name = in.readString();
        this.zodiac_symbol = in.readString();
        this.zodiac_file_path = in.readString();
        this.zodiac_image_path = in.readString();
        this.isSelected = in.readByte() != 0;
    }

    public static final Creator<SunshineBean> CREATOR = new Creator<SunshineBean>() {
        @Override
        public SunshineBean createFromParcel(Parcel source) {
            return new SunshineBean(source);
        }

        @Override
        public SunshineBean[] newArray(int size) {
            return new SunshineBean[size];
        }
    };
}
