package com.example.guru.guru.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.guru.guru.beans.GuruBean;
import com.example.guru.guru.R;
import com.example.guru.guru.activities.AstrologerProfileActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by devrathrathee on 11/06/17 AD.
 */

public class GuruGridAdapter extends BaseAdapter {

    Context context;
    List<GuruBean> guruList = new ArrayList<>();
    LayoutInflater layoutInflater;

    public GuruGridAdapter(Context context, List<GuruBean> guruList) {
        this.context = context;
        this.guruList.addAll(guruList);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return guruList.size();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return guruList.get(i);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder;
        final GuruBean guruBean = guruList.get(i);

        if (view == null) {
            viewHolder = new ViewHolder();
            view = layoutInflater.inflate(R.layout.guru_row, null);
            viewHolder.guruNameTextView = (TextView) view.findViewById(R.id.guruNameTextView);
            viewHolder.guruSkillsTextView = (TextView) view.findViewById(R.id.guruSkillsTextView);
            viewHolder.guruImageView = (ImageView) view.findViewById(R.id.guruImageView);
            viewHolder.guruRatingBar = (RatingBar) view.findViewById(R.id.guruRatingBar);
            viewHolder.guruItemRelativeLayout = (RelativeLayout) view.findViewById(R.id.guruItemLinearLayout);

            view.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) view.getTag();

        Picasso.with(context)
                .load(guruBean.getGuru_photo())
                .into(viewHolder.guruImageView);

        viewHolder.guruNameTextView.setText(guruBean.getGuru_name());

        viewHolder.guruSkillsTextView.setText(guruBean.getGuru_services());

        viewHolder.guruRatingBar.setRating(guruBean.getGuru_rating());

        viewHolder.guruItemRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AstrologerProfileActivity.class);
                intent.putExtra("bean", guruBean);
                context.startActivity(intent);
            }
        });

        return view;
    }

    public class ViewHolder {
        TextView guruNameTextView, guruSkillsTextView;
        ImageView guruImageView;
        RatingBar guruRatingBar;
        RelativeLayout guruItemRelativeLayout;
    }
}
