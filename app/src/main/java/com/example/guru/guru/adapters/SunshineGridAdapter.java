package com.example.guru.guru.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.guru.guru.R;
import com.example.guru.guru.beans.SunshineBean;
import com.example.guru.guru.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by devrathrathee on 13/06/17 AD.
 */

public class SunshineGridAdapter extends BaseAdapter {

    Context context;
    List<SunshineBean> sunshineList = new ArrayList<>();
    LayoutInflater layoutInflater;

    public SunshineGridAdapter(Context context, List<SunshineBean> sunshineList) {
        this.context = context;
        this.sunshineList.addAll(sunshineList);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return sunshineList.size();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return sunshineList.get(i);
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        final SunshineGridAdapter.ViewHolder viewHolder;
        final SunshineBean sunshineBean = sunshineList.get(i);

        if (view == null) {
            viewHolder = new SunshineGridAdapter.ViewHolder();
            view = layoutInflater.inflate(R.layout.sunshine_item, null);
            viewHolder.sunshineNameTextView = (TextView) view.findViewById(R.id.sunshineTextView);
            viewHolder.sunshineImageView = (ImageView) view.findViewById(R.id.sunshineImageView);

            view.setTag(viewHolder);
        } else
            viewHolder = (SunshineGridAdapter.ViewHolder) view.getTag();


        if (sunshineBean.isSelected) {
            setSelectedImage(viewHolder.sunshineImageView, sunshineBean.getZodiac_name());
        } else {
            setUnSelectedImage(viewHolder.sunshineImageView, sunshineBean.getZodiac_name());
        }

        viewHolder.sunshineNameTextView.setText(sunshineBean.getZodiac_name());

        viewHolder.sunshineImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sunshineList.get(i).getZodiac_name().equals("Gemini")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Gemini");
                        ((MainActivity) context).setImage("Gemini", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.gemini_selected));
                    }
                }


                if (sunshineList.get(i).getZodiac_name().equals("Aquarius")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Aquarius");
                        ((MainActivity) context).setImage("Aquarius", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.aquarius_selected));
                    }
                }


                if (sunshineList.get(i).getZodiac_name().equals("Pisces")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Pisces");
                        ((MainActivity) context).setImage("Pisces", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pisces_selected));
                    }
                }

                if (sunshineList.get(i).getZodiac_name().equals("Aries")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Aries");
                        ((MainActivity) context).setImage("Aries", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.aries_selected));
                    }
                }


                if (sunshineList.get(i).getZodiac_name().equals("Taurus")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Taurus");
                        ((MainActivity) context).setImage("Taurus", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.taurus_selected));
                    }
                }

                if (sunshineList.get(i).getZodiac_name().equals("Cancer")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Cancer");
                        ((MainActivity) context).setImage("Cancer", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.cancer_selected));
                    }
                }


                if (sunshineList.get(i).getZodiac_name().equals("Leo")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Leo");
                        ((MainActivity) context).setImage("Leo", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.leo_selected));
                    }
                }


                if (sunshineList.get(i).getZodiac_name().equals("Virgo")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Virgo");
                        ((MainActivity) context).setImage("Virgo", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.virgo_selected));
                    }
                }


                if (sunshineList.get(i).getZodiac_name().equals("Libra")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Libra");
                        ((MainActivity) context).setImage("Libra", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.libra_selected));
                    }
                }
                if (sunshineList.get(i).getZodiac_name().equals("Scorpio")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Scorpio");
                        ((MainActivity) context).setImage("Scorpio", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.scorp_selected));
                    }
                }

                if (sunshineList.get(i).getZodiac_name().equals("Sagittarius")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        setSelected("Sagittarius");
                        ((MainActivity) context).setImage("Sagittarius", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.saggi_selected));
                    }
                }

                if (sunshineList.get(i).getZodiac_name().equals("Capricon")) {

                    if(!sunshineList.get(i).isSelected)
                    {
                        
                        setSelected("Capricon");
                        ((MainActivity) context).setImage("Capricon", sunshineList.get(i).getZodiac_image_path(),sunshineList.get(i).getZodiac_file_path());
                        sunshineList.get(i).setSelected(true);
                        viewHolder.sunshineImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.capri_selected));
                    }
                }
            }
        });


        return view;
    }

    private void setSelected(String sunshine) {
        for (int i = 0; i < sunshineList.size(); i++) {
            if (sunshineList.get(i).getZodiac_name().equals(sunshine))
                sunshineList.get(i).setSelected(true);

            else
                sunshineList.get(i).setSelected(false);
        }
        if(!sunshine.equals(""))
        notifyDataSetChanged();
    }

    private void setSelectedImage(ImageView imageView, String sunshine) {
        if (sunshine.equals("Aquarius"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.aquarius_selected));

        if (sunshine.equals("Gemini"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.gemini_selected));

        if (sunshine.equals("Pisces"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pisces_selected));


        if (sunshine.equals("Aries"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.aries_selected));

        if (sunshine.equals("Taurus"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.taurus_selected));

        if (sunshine.equals("Cancer"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.cancer_selected));

        if (sunshine.equals("Leo"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.leo_selected));

        if (sunshine.equals("Virgo"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.virgo_selected));

        if (sunshine.equals("Libra"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.libra_selected));

        if (sunshine.equals("Scorpio"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.scorp_selected));

        if (sunshine.equals("Sagittarius"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.saggi_selected));

        if (sunshine.equals("Capricon"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.capri_selected));


    }

    private void setUnSelectedImage(ImageView imageView, String sunshine) {
        if (sunshine.equals("Aquarius"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.aquarius_unselected));

        if (sunshine.equals("Gemini"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.gemini_unselected));

        if (sunshine.equals("Pisces"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pisces_unselected));


        if (sunshine.equals("Aries"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.aries_unselected));

        if (sunshine.equals("Taurus"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.taurus_unselected));

        if (sunshine.equals("Cancer"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.cancer_unselected));

        if (sunshine.equals("Leo"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.leo_unselected));

        if (sunshine.equals("Virgo"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.virgo_unselected));

        if (sunshine.equals("Libra"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.libra_unselected));

        if (sunshine.equals("Scorpio"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.scorp_unselected));

        if (sunshine.equals("Sagittarius"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.saggi_unselected));

        if (sunshine.equals("Capricon"))
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.capri_unselected));


    }

    public class ViewHolder {
        TextView sunshineNameTextView;
        ImageView sunshineImageView;
    }
}
