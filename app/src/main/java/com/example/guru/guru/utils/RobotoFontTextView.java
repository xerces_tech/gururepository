package com.example.guru.guru.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by devrath.rathee on 22-06-2017.
 */

public class RobotoFontTextView extends TextView {

    public RobotoFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    public RobotoFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }
    public RobotoFontTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }


    private void applyCustomFont(Context context) {

        Typeface customFont = FontCache.getTypeface("Roboto-Regular.ttf", context);

        setTypeface(customFont);

    }

    public static class FontCache {

        private static HashMap<String, Typeface> fontCache = new HashMap<>();

        public static Typeface getTypeface(String fontname, Context context) {
            Typeface typeface = fontCache.get(fontname);

            if (typeface == null) {
                try {
                    typeface = Typeface.createFromAsset(context.getAssets(), fontname);
                } catch (Exception e) {
                    return null;
                }

                fontCache.put(fontname, typeface);
            }

            return typeface;
        }
    }
}
