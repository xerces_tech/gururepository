package com.example.guru.guru.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.guru.guru.utils.Constants;
import com.example.guru.guru.beans.DrawerBean;
import com.example.guru.guru.adapters.DrawerListAdapter;
import com.example.guru.guru.beans.GuruBean;
import com.example.guru.guru.adapters.GuruGridAdapter;
import com.example.guru.guru.utils.GuruGridView;
import com.example.guru.guru.utils.NetworkUtilities;
import com.example.guru.guru.R;
import com.example.guru.guru.utils.SimpleGestureListener;
import com.example.guru.guru.beans.SunshineBean;
import com.example.guru.guru.adapters.SunshineGridAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements SimpleGestureListener.SimpleGestureFilterListener {

    RequestQueue requestQueue;
    GuruGridView guruGridView,sunshineGridView;
    ProgressDialog progressDialog;
    ImageView imageView;
    SimpleGestureListener listener;
    String months[] = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"};
    TextView selectedSunshineTextView;
    String videoUrl,imageUrl;
    DrawerLayout drawerLayout;
    ListView drawerListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialiseUI();

        requestQueue = Volley.newRequestQueue(this);

        if(NetworkUtilities.isInternet(this)) {
            getGurus();
            getVideos();

        }
        else
            Toast.makeText(this,"Check Internet Connectivity.",Toast.LENGTH_SHORT).show();

    }

    private String getCurrentDate()
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE", Locale.US);
        Calendar calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String week = simpleDateFormat.format(calendar.getTime());

        return  "(" + week+", " + day + " " + getMonth(month) + ", "+ year + ")";
    }

    private ArrayList<DrawerBean> getDrawerArrayList()
    {
        ArrayList<DrawerBean> drawerBeanArrayList = new ArrayList<>();
        drawerBeanArrayList.add(new DrawerBean("Home",R.drawable.home));
        drawerBeanArrayList.add(new DrawerBean("Personalized Horoscope",R.drawable.personalized));
        drawerBeanArrayList.add(new DrawerBean("My Activities",R.drawable.myactivities));
        drawerBeanArrayList.add(new DrawerBean("Profile",R.drawable.profile));
        drawerBeanArrayList.add(new DrawerBean("About",R.drawable.about));
        drawerBeanArrayList.add(new DrawerBean("Terms & Conditions",R.drawable.home));
        drawerBeanArrayList.add(new DrawerBean("FAQs",R.drawable.faq));

        return drawerBeanArrayList;
    }

    private void initialiseUI() {

        listener = new SimpleGestureListener(this,this);

        findViewById(R.id.backImageView).setVisibility(View.VISIBLE);
        findViewById(R.id.backImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(drawerListView);
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerListView = (ListView) findViewById(R.id.drawerListView);
        guruGridView = (GuruGridView) findViewById(R.id.guruGridView);
        sunshineGridView = (GuruGridView) findViewById(R.id.sunshineGridView);
        imageView  = (ImageView) findViewById(R.id.imageView);
        selectedSunshineTextView = (TextView) findViewById(R.id.selectedSunshineTextView);

        DrawerListAdapter drawerListAdapter = new DrawerListAdapter(this,getDrawerArrayList());
        drawerListView.setAdapter(drawerListAdapter);
        drawerListAdapter.notifyDataSetChanged();

        drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                drawerLayout.closeDrawer(drawerListView);

                switch (position)
                {
                    case 1 : startActivity(new Intent(MainActivity.this,Guructivity.class));
                             break;

                    case 2 : startActivity(new Intent(MainActivity.this,MyActivities.class));
                              break;

                    case 3 : startActivity(new Intent(MainActivity.this,MyProfileActivity.class));
                              break;

                    case 4 : startActivity(new Intent(MainActivity.this,AboutActivity.class));
                              break;

                    case 5 : startActivity(new Intent(MainActivity.this,TermsConditionsActivity.class));
                        break;

                    case 6 : startActivity(new Intent(MainActivity.this,FaqActivity.class));
                        break;
                }
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkUtilities.isInternet(MainActivity.this)) {

                    if(videoUrl != null) {
                        if (!videoUrl.equals("")) {
                            Intent intent = new Intent(MainActivity.this, VideoActivity.class);
                            intent.putExtra("url", videoUrl);
                            startActivity(intent);
                        } else
                            Toast.makeText(MainActivity.this, "Video not available.", Toast.LENGTH_SHORT).show();
                    }else
                        Toast.makeText(MainActivity.this, "Select Sunshine.", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(MainActivity.this,"Check Internet Connectivity.",Toast.LENGTH_SHORT).show();

            }
        });


    }

    private String getMonth(int index)
    {
        return months[index];
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me){
        this.listener.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction) {

        switch (direction) {

            case SimpleGestureListener.SWIPE_LEFT :
                    drawerLayout.closeDrawer(drawerListView);
                    startActivity(new Intent(this,Guructivity.class));
                break;

        }

    }

    @Override
    public void onBackPressed() {

        if(drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(drawerListView);

        else
            super.onBackPressed();
    }

    @Override
    public void onDoubleTap() {

    }

    public void setImage(String sunshine,String url,String videoUrl)
    {
        this.imageUrl = url;
        this.videoUrl = videoUrl;

        selectedSunshineTextView.setText(sunshine + getCurrentDate());

        if(NetworkUtilities.isInternet(MainActivity.this))
            Picasso.with(MainActivity.this).load(url).into(imageView);

        else
            Toast.makeText(this,"Check Internet Connectivity.",Toast.LENGTH_SHORT).show();


    }

    private void getVideos()
    {
        progressDialog.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Constants.DAILY_VIDEO_URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        ArrayList<SunshineBean> sunshineBeanArrayList = new Gson().fromJson(response.toString(), new TypeToken<List<SunshineBean>>(){}.getType());

                        SunshineGridAdapter adapter = new SunshineGridAdapter(MainActivity.this,sunshineBeanArrayList);
                        sunshineGridView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.i("Error : ",error.toString());
                    }
                });
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }

    private void getGurus()
    {
        progressDialog.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Constants.GURU_LIST_URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        ArrayList<GuruBean> guruBeanArrayList = new Gson().fromJson(response.toString(), new TypeToken<List<GuruBean>>(){}.getType());
                        GuruGridAdapter adapter = new GuruGridAdapter(MainActivity.this,guruBeanArrayList);
                        guruGridView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.i("Error : ",error.toString());
                    }
                });

        requestQueue.add(jsonArrayRequest);
    }
}
