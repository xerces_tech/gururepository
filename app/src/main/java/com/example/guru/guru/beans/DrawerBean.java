package com.example.guru.guru.beans;

/**
 * Created by devrathrathee on 24/06/17 AD.
 */

public class DrawerBean {

    String name;
    int image;

    public DrawerBean(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
