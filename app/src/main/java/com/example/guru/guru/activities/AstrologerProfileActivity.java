package com.example.guru.guru.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.guru.guru.beans.GuruBean;
import com.example.guru.guru.R;
import com.squareup.picasso.Picasso;

public class AstrologerProfileActivity extends AppCompatActivity {

    ImageView guruProfileImageView;
    TextView guruNameTextView,guruSkillsTextView,guruDescTextView,guruExperienceTextView;
    RatingBar guruRatingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        RelativeLayout headerRelativeLayout = (RelativeLayout) findViewById(R.id.profileHeader);
        (headerRelativeLayout.findViewById(R.id.appNameTextView)).setVisibility(View.GONE);
        (headerRelativeLayout.findViewById(R.id.profileTextView)).setVisibility(View.VISIBLE);
        ((TextView)headerRelativeLayout.findViewById(R.id.profileTextView)).setText("Astrologer Profile");

        findViewById(R.id.backToPreviousImageView).setVisibility(View.VISIBLE);
        findViewById(R.id.backToPreviousImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        guruProfileImageView = (ImageView) findViewById(R.id.profileImageView);
        guruNameTextView = (TextView) findViewById(R.id.guruName_ProfileTextView);
        guruSkillsTextView = (TextView) findViewById(R.id.guruSkills_ProfileTextView);
        guruExperienceTextView = (TextView) findViewById(R.id.guruExperience_ProfileTextView);
        guruDescTextView = (TextView) findViewById(R.id.guruDescTextView);
        guruRatingBar = (RatingBar) findViewById(R.id.guruRating_Profile);

        GuruBean guruBean = getIntent().getParcelableExtra("bean");
        guruNameTextView.setText(guruBean.getGuru_name());
        guruSkillsTextView.setText(Html.fromHtml("Speciality: " + "<b>" + guruBean.getGuru_services() + "</b>"));
        guruDescTextView.setText(guruBean.getGuru_description());
        guruExperienceTextView.setText("Experience: " + guruBean.getGuru_experience() +  " years" );
        guruRatingBar.setRating(guruBean.getGuru_rating());

        Picasso.with(this)
                .load(guruBean.getGuru_photo())
                .into(guruProfileImageView);

    }
}
