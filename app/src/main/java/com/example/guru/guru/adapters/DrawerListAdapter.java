package com.example.guru.guru.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.guru.guru.beans.DrawerBean;
import com.example.guru.guru.R;

import java.util.ArrayList;

/**
 * Created by devrathrathee on 24/06/17 AD.
 */

public class DrawerListAdapter extends BaseAdapter {

    Context context;
    ArrayList<DrawerBean> drawerBeanArrayList = new ArrayList<>();
    LayoutInflater layoutInflater;

    public DrawerListAdapter(Context context,ArrayList<DrawerBean> drawerBeanArrayList)
    {
        this.context = context;
        this.drawerBeanArrayList.addAll(drawerBeanArrayList);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return drawerBeanArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return drawerBeanArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerBean bean = drawerBeanArrayList.get(position);
        ViewHolder viewHolder = null;

        if(viewHolder == null)
        {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.drawer_row,null);
            viewHolder.itemTextView = (TextView) convertView.findViewById(R.id.itemTextView);
            viewHolder.itemImageView = (ImageView) convertView.findViewById(R.id.itemImageView);
            viewHolder.drawerItemRelativeLayout = (RelativeLayout) convertView.findViewById(R.id.drawerItemRelativeLayout);

            convertView.setTag(viewHolder);
        }

        viewHolder.itemTextView.setText(bean.getName());
        if(bean.getImage() != 0)
        viewHolder.itemImageView.setImageDrawable(context.getResources().getDrawable(bean.getImage()));

        return convertView;
    }

    class ViewHolder{
        TextView itemTextView;
        ImageView itemImageView;
        RelativeLayout drawerItemRelativeLayout;
    }
}
